terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

# Configure the AWS Provider
provider "aws" { 
  profile = "default"
  region  = var.region
}

resource "aws_instance" "mecsys" {
    ami             = "ami-03d315ad33b9d49c4"
    instance_type   = "t2.micro"

    tags = {
        Name = var.instance_name
        version = var.instance_version
    }
}

resource "aws_eip" "ip" {
    vpc         = true
    instance    = aws_instance.mecsys.id
}

output "ip" {
    value = aws_eip.ip.public_ip
}
